'''
stat.ST_MTIME
Time of last modification.

stat.ST_CTIME
The ctime as reported by the operating system. On some systems (like Unix) is the 
time of the last metadata change, and, on others (like Windows), is the creation time 
(see platform documentation for details).
pip install -U pip setuptools
HOW TO USE:
itso_collator.py <RTD_ID> <output_file.txt>

itso_collator.py 1485762 output_file.txt

Optionally, if you want to redirect the stdout and stderr 
(ie. content that is printed to the screen) you can append > logs.txt 2>&1
as such:

itso_collator.py 1485762 output_file.txt > logs.txt 2>&1

the location of logs.txt will be in C:\\Users\\<USER ID>
185 bytes per row(record) currently

To add/fix:
Automatic logs saved to a file

Option 1: Print comapct messages automatically with the graph (compact mpl)
Option 2: Messages per file (option to add filename at the end)

fix. running time total ...

'''
#Author: Marcin Rojek
import matplotlib.pyplot as plt
import os
import sys
import glob
import datetime
from datetime import datetime, timedelta

import matplotlib.dates as mdates

yval=[]
y2val=[]
y3val=[]
xval=[]

start_time=datetime(1997,1,1,0,0)
end_time =datetime(1997,1,1,0,0)
error_date=datetime(2017,10,12,0,0)
error_date2=datetime(2017,10,11,0,0)



ITSO_BASE_DATE = datetime(1997,1,1,0,0,0)

error_count=0

if len(sys.argv) < 3:
    print("Please enter RTD ID and output file \nFor example testV2.py 4857666 output.txt \n")
else:
    #HOPS_search_dir = '\\10.28.41.117\d$\ItsoFiles\HopsSender_ToHops'
    print("RTD ID:",str(sys.argv[1]),"\n")
    search_dir = '\\\\me-nts1\\users\\202490\\Desktop\\ITSO\\ITSO Message Collator\\u92bark\\'
    print("Search path:",search_dir)
    print("Loading files into array")
    files_temp = [i for i in os.listdir(search_dir) if os.path.isfile(os.path.join(search_dir,i)) and \
             str(sys.argv[1]) in i]
    print("Files loaded. Sorting files by mtime")
    files = sorted(files_temp, key=lambda x: datetime.fromtimestamp(os.stat(search_dir+x).st_mtime)  )
    #see top regarding sorting files by ctime and mtime

    #prints the whole object comma separated list 
    print("\n".join(files))

    print("Sorted number of files:",len(files))
    print("Now,writing to file:",str(sys.argv[2]))
    #for index, item in enumerate(it):
    for index,z in enumerate(files):
        with open(search_dir+z) as f:
            #outsearch_dir= search_dir + str(sys.argv[2]) 
            #output_f = open(outsearch_dir,"a")
            print(f)
            for line in f:
                if ("<ITSO_Message_Code>") in line:
                    code =line.split(">",1)[1][:4]
                    yval.extend([code])
                    print(code)
                if "<ITSO_DTS>" in line:
                    date =line.split(">",1)[1][:6]
                    #minutes since ITSO_BASE_DATE
                    int_date_minutes = int(date, 16)
                    datetime_xml_date = ITSO_BASE_DATE + timedelta(minutes=int_date_minutes)
                    strdate = str(datetime_xml_date)
                    xval.extend([datetime_xml_date])
                    #print("Before:"+str(datetime_xml_date))
                    #output_f.write(str(datetime.fromtimestamp(os.stat(f.name).st_mtime)))  
                    if index==0:
                        start_time=datetime_xml_date
                    if index==(len(files)-1):
                        end_time=datetime_xml_date
                    if datetime_xml_date==error_date:
                        error_count=error_count+1
                        print(yval[(len(yval)-1)])
                        yval[(len(yval)-1)]="0450"
                        datetime_xml_date=datetime.fromtimestamp(os.stat(f.name).st_mtime)
                        xval[(len(yval)-1)]=datetime_xml_date
                    #print("After"+str(datetime_xml_date))
                    file_date = datetime.fromtimestamp(os.stat(f.name).st_mtime)
                    delta= file_date - datetime_xml_date
                    
                    print("delta:",delta.total_seconds())
                    delta_ts=int(delta.total_seconds())
                    datetime_xml_date_add_delta= datetime_xml_date + timedelta(seconds=delta_ts)
                    y3val.extend([datetime_xml_date_add_delta])
                    #dt = datetime.combine(date.today(), time(23, 55)) + timedelta(minutes=30)
                    #time_diff = file_date
                    #time_diff1 = file_date - timedelta(minutes=int_date_minutes)
                    #time_diff= datetime_xml_date + timedelta(minutes=time_diff1)
                    y2val.extend([file_date])

            print(index)
            #output_f.close()
    print("FINISHED")

print("Running Time Total:"+str(end_time-start_time))
print("error_count:"+ str(error_count))
#print(y2val)
#GRAPH
xs = mdates.date2num(xval)

hfmt = mdates.DateFormatter('%Y-%m-%d %H:%M:%S')
hfmt2= mdates.DateFormatter('%H:%M:%S')

fig = plt.figure()

ax = fig.add_subplot(1,1,1)
ax.xaxis.set_major_formatter(hfmt)
plt.setp(ax.get_xticklabels(), rotation=15)

ax2=ax.twinx()
ax2.yaxis.set_major_formatter(hfmt)
y2s=mdates.date2num(y2val)

#ax3=ax.twinx()
#ax3.yaxis.set_major_formatter(hfmt2)
#y3s=mdates.date2num(y3val)


ax.plot(xs, yval,'ro')
ax2.plot(xs,y2s,'bo')
#ax3.plot(xs,y3s,'go')

plt.show()

#https://stackoverflow.com/questions/9103166/multiple-axis-in-matplotlib-with-different-scales