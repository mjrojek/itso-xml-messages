'''
stat.ST_MTIME
Time of last modification.

stat.ST_CTIME
The “ctime” as reported by the operating system. On some systems (like Unix) is the 
time of the last metadata change, and, on others (like Windows), is the creation time 
(see platform documentation for details).

HOW TO USE:
itso_collator.py <RTD_ID> <output_file.txt>

itso_collator.py 1485762 output_file.txt

Optionally, if you want to redirect the stdout and stderr 
(ie. content that is printed to the screen) you can append > logs.txt 2>&1
as such:

itso_collator.py 1485762 output_file.txt > logs.txt 2>&1

the location of logs.txt will be in C:\\Users\\<USER ID>
185 bytes per row(record) currently

SAMPLE OUTPUT:
ITSO_M_C:0803   M_D:2017-08-02 14:49:00 F_D:2017-08-02 14:49:02.726054  F:2_UNKNOWN_FIELD_91a5ee10-48f3-4b42-8a

ITSO_M_C = ITSO MESSAGE CODE
M_D = (ITSO) Message datestamp 
F_D = (Windows) File modification datestamp
F = Filename

'''
#Author: Marcin Rojek
import os
import sys
import glob
import datetime
from datetime import datetime, timedelta

if len(sys.argv) < 3:
    print("Please enter RTD ID and output file \nFor example itso_XYZ.py 4857666 output.txt \n")
else:
    #HOPS_search_dir = '\\10.28.41.117\d$\ItsoFiles\HopsSender_ToHops'
    # 1-Jan-1997 00:00:00
    ITSO_BASE_DATE = datetime(1997,1,1,0,0,0)

    print("RTD ID:",str(sys.argv[1]),"\n")
    search_dir = '\\\\me-nts1\\users\\202490\\Desktop\\ITSO\\ITSO Message Collator\\ley3\\'

    files_temp = [i for i in os.listdir(search_dir) if os.path.isfile(os.path.join(search_dir,i)) and \
             str(sys.argv[1]) in i]
    
    files = sorted(files_temp, key=lambda x: datetime.fromtimestamp(os.stat(search_dir+x).st_mtime)   )
    #see top regarding sorting files by ctime and mtime

    print("\n".join(files))

    print("Sorted number of files:",len(files))
    print("Now,writing to file:",str(sys.argv[2]))

    for z in files:
        with open(search_dir+z) as f:
            outsearch_dir= search_dir + str(sys.argv[2]) 
            output_f = open(outsearch_dir,"a")
            for line in f:
                if ("<ITSO_Message_Code>") in line:
                    code =line.split(">",1)[1][:4]
                    output_f.write("ITSO_M_C:")
                    output_f.write(code)
                    output_f.write("\t")
                if "<ITSO_DTS>" in line:
                    date =line.split(">",1)[1][:6]
                    #minutes since ITSO_BASE_DATE
                    int_date_minutes = int(date, 16)
                    new_date = ITSO_BASE_DATE + timedelta(minutes=int_date_minutes)
                    strdate = str(new_date)
                    output_f.write("M_D:")
                    output_f.write(strdate)
                    output_f.write("\t")
                    output_f.write("F_D:")
                    output_f.write(str(datetime.fromtimestamp(os.stat(f.name).st_mtime)))
                    output_f.write("\t")
                    output_f.write("F:")
                    output_f.write(f.name[46:91])
                    output_f.write("\n")
            output_f.close()
    print("FINISHED")