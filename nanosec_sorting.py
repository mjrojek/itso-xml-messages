import os
import sys
import datetime

search_dir = '\\\\me-nts1\\users\\202490\\Desktop\\ITSO\\ITSO Message Collator\\ley3\\'

#PRINT ALL files in the search_dir directory
files = sorted(os.listdir(search_dir), key=lambda x: os.path.getctime(os.path.join(search_dir, x)))
#print("\n".join(files))

#Print all files with a specified argument
files2 = [i for i in os.listdir(search_dir) if os.path.isfile(os.path.join(search_dir,i)) and \
		str(sys.argv[1]) in i]
print("Printing Files with that contain:" + sys.argv[1])
print("\n".join(files2))

print("Printing timestamp (nanosec) precision of files that contain specified argument"+sys.argv[1])
for z in files2:
	print(z)
	print(datetime.datetime.fromtimestamp(os.stat(search_dir+z).st_ctime))


#Sort files with specified argument
files3 = sorted(files2, key=lambda x: datetime.datetime.fromtimestamp(os.stat(search_dir+x).st_ctime)   )
print("Sorted nanoseconds\n")
for a in files3:
	print(a)
	print(datetime.datetime.fromtimestamp(os.stat(search_dir+a).st_ctime))

print("number of files:",len(files2))